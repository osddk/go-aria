// Copyright (c) 2014 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package auth

import (
	"bitbucket.org/osddk/go-aria/auth/storage"
)

type Adapter interface {
	Authenticate() (*Result, error)
}

type Storage interface {
	Clear()
	IsEmpty() bool
	Read() interface{}
	Write(v interface{})
}

type Auth struct {
	Storage Storage
}

// NewAuth allocates and returns a new Auth.
func NewAuth() *Auth {
	return &Auth{Storage: storage.NewVolatileStorage()}
}

// Authenticate performs authentication using specified adapter.
// If authentication is a success the identity object is written to the storage.
func (a *Auth) Authenticate(adapter Adapter) (*Result, error) {
	// Authenticate using provided adapter.
	r, err := adapter.Authenticate()
	if err != nil {
		return nil, err
	}

	// Make sure any existing authentications are cleared.
	if a.HasIdentity() {
		a.ClearIdentity()
	}

	// Only store identity if authentication succeeded.
	if r.IsValid() {
		a.Storage.Write(r.Identity)
	}

	return r, nil
}

// Identity retrieves the stored identity object.
func (a *Auth) Identity() interface{} {
	return a.Storage.Read()
}

// HasIdentity checks if we have a an identity in storage.
func (a *Auth) HasIdentity() bool {
	return !a.Storage.IsEmpty()
}

// ClearIdentity deletes any identity in storage.
func (a *Auth) ClearIdentity() {
	a.Storage.Clear()
}
