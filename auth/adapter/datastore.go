// Copyright (c) 2014 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package adapter

import (
	"appengine"
	"appengine/datastore"
	"bitbucket.org/osddk/go-aria/auth"
)

type DatastoreAdapter struct {
	context          appengine.Context // appengine context
	Identity         string            // identity used for authentication
	Credential       string            // credential used for authentication
	Entity           string            // datastore entity name
	IdentityColumn   string            // datastore identity property name
	CredentialColumn string            // datastore credential property name
}

// NewDatastoreAdapter allocates and returns a new DatastoreAdapter.
func NewDatastoreAdapter(c appengine.Context) *DatastoreAdapter {
	return &DatastoreAdapter{
		context:          c,
		IdentityColumn:   "Username",
		CredentialColumn: "Password",
	}
}

// Authenticate is called by Auth when passed to its Authenticate call.
func (da *DatastoreAdapter) Authenticate() (*auth.Result, error) {
	type Account struct {
		Id int
	}

	q := datastore.NewQuery(da.Entity).
		Project("Id").
		Filter(da.IdentityColumn+" =", da.Identity).
		Filter(da.CredentialColumn+" =", da.Credential).
		Limit(1)

	var account []Account
	if _, err := q.GetAll(da.context, &account); err != nil {
		return nil, err
	}

	var r *auth.Result
	if len(account) == 1 {
		r = auth.NewResult(auth.Success, account[0].Id)
	} else {
		r = auth.NewResult(auth.Failure, nil)
	}
	return r, nil
}
