// Copyright (c) 2014 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package render

import (
	"encoding/json"
	"io"
)

func RenderAsJson(w io.Writer, result interface{}) (err error) {
	enc := json.NewEncoder(w)
	switch t := result.(type) {
	default:
		err = enc.Encode(result)
	case error:
		type Error struct {
			Message string `json:"message"`
		}
		err = enc.Encode(&Error{Message: t.Error()})
	}
	return err
}
