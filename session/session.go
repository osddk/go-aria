// Copyright (c) 2014 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package session

import (
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"
)

const (
	DefaultName      = "ariasid" // Default session name.
	DefaultIdSize    = 32        // Default number of random bytes to use for id generation.
	DefaultMaxAge    = 900       // Default number of seconds before session expires.
	DefaultNamespace = ""        // Default session container namespace.
)

var (
	ErrInvalidSessionStorage = errors.New("Invalid session storage")
)

type ContainerKey struct {
	Namespace, Key string
}

type Container map[ContainerKey]interface{}

type Session struct {
	Name      string        // name of session
	Id        string        // id identifying unique session
	MaxAge    time.Duration // number of seconds before session expires
	Container               // session data container

	storage Storage    // object to load/save session container
	ns      *Namespace // default session namespace
}

func NewSession(st Storage) *Session {
	c := make(Container)
	return &Session{
		Name:      DefaultName,
		Id:        "",
		MaxAge:    DefaultMaxAge,
		Container: c,
		storage:   st,
		ns:        &Namespace{ns: DefaultNamespace, c: c},
	}
}

func (s *Session) AttachCookie(w http.ResponseWriter) error {
	c := &http.Cookie{
		Name:  s.Name,
		Value: s.Id,
		Path:  "/",
	}
	http.SetCookie(w, c)
	return nil
}

func (s *Session) AttachHeader(w http.ResponseWriter) error {
	h := w.Header()
	h.Add("x-"+s.Name, s.Id)
	return nil
}

func (s *Session) GenerateId(size int64) error {
	b := make([]byte, size)
	n, err := io.ReadFull(rand.Reader, b)
	if err != nil {
		return err
	}
	if n != len(b) {
		return fmt.Errorf("Only %d bytes generated; requested %d bytes", n, size)
	}
	s.Id = base64.StdEncoding.EncodeToString(b)
	return nil
}

func (s *Session) RegenerateId() error {
	return s.GenerateId(DefaultIdSize)
}

func (s *Session) Load() error {
	st, ok := s.storage.(Storage)
	if !ok {
		return ErrInvalidSessionStorage
	}
	return st.Load(s)
}

func (s *Session) Save() error {
	st, ok := s.storage.(Storage)
	if !ok {
		return ErrInvalidSessionStorage
	}
	return st.Save(s)
}

func (s *Session) SetIdFromCookie(r *http.Request) error {
	c, err := r.Cookie(s.Name)
	if err != nil && err != http.ErrNoCookie {
		return err
	}

	if c != nil {
		s.Id = c.Value
		s.Load()
	}

	return nil
}

func (s *Session) SetIdFromHeader(r *http.Request) {
	h := r.Header.Get("x-" + s.Name)
	if h != "" {
		s.Id = h
		s.Load()
	}
}

func (s *Session) Namespace(ns string) *Namespace {
	return &Namespace{ns: ns, c: s.Container}
}

func (s *Session) Get(key string) interface{} {
	return s.ns.Get(key)
}

func (s *Session) Set(key string, value interface{}) {
	s.ns.Set(key, value)
}

func (s *Session) Del(key string) {
	s.ns.Del(key)
}

type Namespace struct {
	ns string
	c  Container
}

func (n *Namespace) Get(key string) interface{} {
	k := ContainerKey{Namespace: n.ns, Key: key}
	return n.c[k]
}

func (n *Namespace) Set(key string, value interface{}) {
	k := ContainerKey{Namespace: n.ns, Key: key}
	n.c[k] = value
}

func (n *Namespace) Del(key string) {
	k := ContainerKey{Namespace: n.ns, Key: key}
	delete(n.c, k)
}
