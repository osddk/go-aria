// Copyright (c) 2014 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package environment

type Environment struct {
	Var map[string]interface{}
}

var single *Environment

func NewEnvironment() (env *Environment, err error) {
	if single == nil {
		single = &Environment{
			Var: make(map[string]interface{}),
		}
	}
	return single, nil
}

// func (env *Environment) Get(key string) interface{} {
// 	return env.data[key]
// }

// func (env *Environment) Set(key string, val interface{}) {
// 	env.data[key] = val
// }
